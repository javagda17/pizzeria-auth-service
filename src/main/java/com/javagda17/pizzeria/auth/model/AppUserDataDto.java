package com.javagda17.pizzeria.auth.model;

import lombok.Data;

@Data
public class AppUserDataDto {
    private String username;
    private String password;
}

/*
{
    "username" : "a",
    "passport" : "b",
    "password" : "c"
}

dto.username = "a"
dto.password = "c";
dto.password = null;
 */
