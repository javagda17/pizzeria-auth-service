package com.javagda17.pizzeria.auth.controller;

import com.javagda17.pizzeria.auth.model.AppUser;
import com.javagda17.pizzeria.auth.model.AppUserDataDto;
import com.javagda17.pizzeria.auth.service.AppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class AppUserController {
    @Autowired
    private AppUserService appUserService;

    @GetMapping("/get/{id}")
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<AppUser> get(@PathVariable(name = "id") Long id) {
        Optional<AppUser> appUserOptional = appUserService.getById(id);
        if (appUserOptional.isPresent()) {
            return ResponseEntity.ok(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/register")
    public ResponseEntity<AppUser> register(@RequestBody AppUserDataDto appUserDataDto) {
        Optional<AppUser> appUserOptional = appUserService.register(appUserDataDto.getUsername(), appUserDataDto.getPassword());

        if (appUserOptional.isPresent()) {
            return ResponseEntity.ok(appUserOptional.get());
        }
        return ResponseEntity.badRequest().build();
    }
}
