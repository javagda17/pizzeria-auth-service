package com.javagda17.pizzeria.auth.repository;

import com.javagda17.pizzeria.auth.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
    UserRole getByName(String role_user);

    Optional<UserRole> findByName(String name);
}
