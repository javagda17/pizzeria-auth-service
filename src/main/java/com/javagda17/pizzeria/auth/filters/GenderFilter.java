package com.javagda17.pizzeria.auth.filters;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GenderFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        // Bearer eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJKYSIsInN1YiI6ImFkbWluIiwiQXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfVVNFUiJdLCJpYXQiOjE1NDc3NTQ0NDcsImV4cCI6MTU0Nzc1ODA0N30.W9l9Jn6iV9xlQ1m9kn77yVglZQhOMajKQ09YNPSQetCYe8Kw4kM-UqybWXz3xaNI37yNUM_o6CQyn2lwCjy24w
        String header = request.getHeader("Authorization");

        if (header == null || !header.startsWith("Bearer ")) {
            chain.doFilter(request, response);        // If not valid, go to the next filter.
            return;
        }

        String token = header.replace("Bearer ", "");
        // eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJKYSIsInN1YiI6ImFkbWluIiwiQXV0aG9yaXRpZXMiOlsiUk9MRV9BRE1JTiIsIlJPTEVfVVNFUiJdLCJpYXQiOjE1NDc3NTQ0NDcsImV4cCI6MTU0Nzc1ODA0N30.W9l9Jn6iV9xlQ1m9kn77yVglZQhOMajKQ09YNPSQetCYe8Kw4kM-UqybWXz3xaNI37yNUM_o6CQyn2lwCjy24w

        Claims claims = Jwts.parser()
                .setSigningKey("JwtSecretKey".getBytes())
                .parseClaimsJws(token)
                .getBody();
        // claims to:
        // nazwa użytkownika
        // 'mapa' claim
        // 'Authorities' jako jeden z claim

        String username = claims.getSubject();
        if (username.endsWith("a")) {
            response.addHeader("rodzaj", "niemeski");
        } else {
            response.addHeader("rodzaj", "meski");
        }

        chain.doFilter(request, response);
    }
}
