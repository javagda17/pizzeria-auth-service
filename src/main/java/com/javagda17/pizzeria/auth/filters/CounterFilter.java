package com.javagda17.pizzeria.auth.filters;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CounterFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String headerValue = request.getHeader("counter");
        if (headerValue == null) {
            response.addHeader("counter", "1");
            chain.doFilter(request, response);
            return;
        }

        int counterValue = Integer.parseInt(headerValue);
        counterValue++;

        response.addHeader("counter", "" + counterValue);

        chain.doFilter(request, response);
    }
}
