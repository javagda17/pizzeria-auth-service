package com.javagda17.pizzeria.auth.filters;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javagda17.pizzeria.auth.model.AppUserDataDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;

        // http://localhost:8080/login
        // /login
        setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login"));
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            InputStream daneZRamki = request.getInputStream(); // <- request body

            // przeczytaj z wejścia ( z ramki http) dane i przetwórz je = w wyniku otrzymamy obiekt klasy
            // request
            // json -> obiekt dto
            AppUserDataDto dto = new ObjectMapper().readValue(daneZRamki, AppUserDataDto.class); // <- w body ma być taki obiekt

            UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(
                            dto.getUsername(),
                            dto.getPassword(),
                            Collections.emptyList());

            return authenticationManager.authenticate(token);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        throw new UsernameNotFoundException("Could not authorize.");
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException, ServletException {
        // wygeneruj i zwróć token
        Long now = System.currentTimeMillis();
        String token = Jwts.builder()
                .setIssuer("Ja")
                .setSubject(auth.getName())
                .claim("Authorities", auth.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + 3600 * 1000))  // in milliseconds - 1h
                .signWith(SignatureAlgorithm.HS512, "JwtSecretKey".getBytes())
                .compact();

        response.addHeader("Authorization", "Bearer " + token);
    }
}
