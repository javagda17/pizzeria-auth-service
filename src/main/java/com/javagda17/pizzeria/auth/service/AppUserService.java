package com.javagda17.pizzeria.auth.service;

import com.javagda17.pizzeria.auth.exceptions.UserNotFound;
import com.javagda17.pizzeria.auth.model.AppUser;
import com.javagda17.pizzeria.auth.model.UserRole;
import com.javagda17.pizzeria.auth.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // todo: wyszukiwanie użytkownika po nazwie użytkownika
    public Optional<AppUser> getByUsername(String username) {
        return appUserRepository.findByUsername(username);
    }

    // todo: wyszukiwanie uprawnień użytkownika podając id użytkownika jako parametr
    public List<UserRole> getRolesByUsername(String username) {
        Optional<AppUser> userOptional = getByUsername(username);
        if (userOptional.isPresent()) {
            return new LinkedList<>(userOptional.get().getUserRoles());
        }
        throw new UserNotFound();
    }

    // todo: wyszukiwanie użytkownika podając id użytkownika jako parametr
    public Optional<AppUser> getById(Long id) {
        return appUserRepository.findById(id);
    }

    // todo: dodawanie użytkownika z (domyślnie) ustawieniami ROLE_USER
    public Optional<AppUser> register(String username, String password) {
        if (!getByUsername(username).isPresent()) {
            AppUser appUser = new AppUser();
            appUser.setUsername(username);
            appUser.setPassword(bCryptPasswordEncoder.encode(password)); // todo: szyfrowanie.
            appUser.setUserRoles(new HashSet<>(Arrays.asList(userRoleService.getUserRole())));

            appUser = appUserRepository.save(appUser);
            return Optional.of(appUser);
        }
        return Optional.empty();
    }
}
