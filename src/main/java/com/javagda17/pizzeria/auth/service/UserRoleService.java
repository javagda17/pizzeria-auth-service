package com.javagda17.pizzeria.auth.service;

import com.javagda17.pizzeria.auth.model.UserRole;
import com.javagda17.pizzeria.auth.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;

    // todo: metoda do pobrania uprawnienia (encji z bazy) o nazwie ROLE_USER
    public UserRole getUserRole(){
        return userRoleRepository.getByName("ROLE_USER");
    }

    // todo: metoda do pobrania uprawnienia (encji z bazy) o nazwie ROLE_ADMIN
    public UserRole getAdminRole(){
        return userRoleRepository.getByName("ROLE_ADMIN");
    }

}
