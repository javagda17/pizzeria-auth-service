package com.javagda17.pizzeria.auth.configuration;

import com.javagda17.pizzeria.auth.filters.CounterFilter;
import com.javagda17.pizzeria.auth.filters.GenderFilter;
import com.javagda17.pizzeria.auth.filters.JwtAuthenticationFilter;
import com.javagda17.pizzeria.auth.filters.JwtAutorizationTokenFilter;
import com.javagda17.pizzeria.auth.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.WebSecurityConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .addFilter(new JwtAuthenticationFilter(authenticationManagerBean())) // < - to jest filtr który loguje
                .addFilterBefore(new CounterFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(new JwtAutorizationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
                .addFilterAfter(new GenderFilter(), UsernamePasswordAuthenticationFilter.class)
                // filtry
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
//                .antMatchers(HttpMethod.POST, "/login", "/register").permitAll()
                // filtr się włącza
//                .and().formLogin().loginPage("/login")
                .anyRequest().authenticated();
    }

    @Autowired
    private LoginService loginService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
